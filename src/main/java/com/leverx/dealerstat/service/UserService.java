package com.leverx.dealerstat.service;

import com.leverx.dealerstat.dto.request.UserRequest;
import com.leverx.dealerstat.dto.response.TraderRatingResponse;
import com.leverx.dealerstat.dto.response.UserResponse;
import com.leverx.dealerstat.entity.User;

import java.util.List;

public interface UserService {
    boolean existsByEmail(String email);

    UserResponse getById(Long id);

    User findById(Long id);

    User findByEmail(String email);

    List<UserResponse> getAllTraders();

    User saveNew(UserRequest userRequest);

    UserResponse saveConfirmed(User user);

    UserResponse update(User user);

    void deleteById(Long id);

    String saveInRedis(User user);

    User findByTokenFromRedis(String token);

    void deleteByTokenFromRedis(String token);

    boolean isEnabled(String email);

    double getTraderRatingById(Long id);

    List<TraderRatingResponse> getTraderTopByRatings();
}

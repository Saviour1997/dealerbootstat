package com.leverx.dealerstat.service;

import com.leverx.dealerstat.dto.request.GameObjectRequest;
import com.leverx.dealerstat.dto.response.GameObjectResponse;
import com.leverx.dealerstat.entity.GameObject;

import java.util.List;

public interface GameObjectService {
    GameObject findById(Long id);

    GameObjectResponse getById(Long id);

    List<GameObjectResponse> getAll();

    List<GameObjectResponse> getAllByTraderId(Long id);

    List<GameObjectResponse> getAllByGameId(Long id);

    GameObjectResponse saveNew(GameObjectRequest gameObjectRequest);

    GameObjectResponse update(GameObjectRequest gameObjectRequest);

    void deleteById(Long id);
}

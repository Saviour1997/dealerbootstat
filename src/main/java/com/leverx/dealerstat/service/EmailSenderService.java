package com.leverx.dealerstat.service;

import org.springframework.mail.SimpleMailMessage;


public interface EmailSenderService {
    void sendEmail(SimpleMailMessage message);

    SimpleMailMessage createConfirmSignUpMessage(String toEmail, String token);

    SimpleMailMessage createConfirmNewPasswordMessage(String toEmail, String token);
}

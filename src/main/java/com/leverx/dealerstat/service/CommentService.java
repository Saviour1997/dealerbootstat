package com.leverx.dealerstat.service;

import com.leverx.dealerstat.dto.request.CommentRequest;
import com.leverx.dealerstat.dto.response.CommentResponse;
import com.leverx.dealerstat.entity.Comment;

import java.util.List;

public interface CommentService {
    List<CommentResponse> getAllComments();

    List<CommentResponse> getAllApprovedCommentsByTraderId(Long traderId);

    List<CommentResponse> getAllUnapprovedCommentsByTraderId(Long traderId);

    Comment findById(Long id);

    CommentResponse getById(Long id);

    CommentResponse saveNew(CommentRequest commentRequest);

    CommentResponse update(Comment comment);

    void approveById(Long id);

    void deleteById(Long id);
}

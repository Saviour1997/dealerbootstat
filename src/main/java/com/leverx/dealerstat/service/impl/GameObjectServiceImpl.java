package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.dto.mapper.GameObjectMapper;
import com.leverx.dealerstat.dto.request.GameObjectRequest;
import com.leverx.dealerstat.dto.response.GameObjectResponse;
import com.leverx.dealerstat.entity.Game;
import com.leverx.dealerstat.entity.GameObject;
import com.leverx.dealerstat.entity.User;
import com.leverx.dealerstat.repository.GameObjectRepository;
import com.leverx.dealerstat.repository.GameRepository;
import com.leverx.dealerstat.repository.UserRepository;
import com.leverx.dealerstat.service.GameObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
public class GameObjectServiceImpl implements GameObjectService {

    private final GameObjectRepository gameObjectRepository;
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final GameObjectMapper gameObjectMapper;

    @Autowired
    public GameObjectServiceImpl(GameObjectRepository gameObjectRepository, GameRepository gameRepository, UserRepository userRepository, GameObjectMapper gameObjectMapper) {
        this.gameObjectRepository = gameObjectRepository;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
        this.gameObjectMapper = gameObjectMapper;
    }

    @Override
    public GameObject findById(Long id) {
        return gameObjectRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public GameObjectResponse getById(Long id) {
        return gameObjectMapper.entityToResponse(gameObjectRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public List<GameObjectResponse> getAll() {
        return gameObjectMapper.entityToResponseList(gameObjectRepository.findAll());
    }

    @Override
    public List<GameObjectResponse> getAllByTraderId(Long id) {
        return gameObjectMapper.entityToResponseList(gameObjectRepository.findAllByTraderId(id));
    }

    @Override
    public List<GameObjectResponse> getAllByGameId(Long id) {
        return gameObjectMapper.entityToResponseList(gameObjectRepository.findAllByGameId(id));
    }

    @Override
    public GameObjectResponse saveNew(GameObjectRequest gameObjectRequest) {
        String gameName = gameObjectRequest.getGameName();
        Long traderId = gameObjectRequest.getTraderId();
        User user = userRepository.findById(traderId).orElseThrow(EntityNotFoundException::new);
        Game game = gameRepository.findByName(gameName)
                .orElseGet(() -> gameRepository.save(Game.builder().name(gameName).build()));
        GameObject gameObject = gameObjectMapper.requestToEntity(gameObjectRequest);
        gameObject.setCreatedAt(LocalDate.now());
        gameObject.setUpdatedAt(LocalDate.now());
        gameObject.setGame(game);
        gameObject.setTrader(user);
        return gameObjectMapper.entityToResponse(gameObjectRepository.save(gameObject));
    }

    @Override
    public GameObjectResponse update(GameObjectRequest gameObjectRequest) {
        GameObject gameObject = findById(gameObjectRequest.getId());
        gameObject.setTitle(gameObjectRequest.getTitle());
        gameObject.setText(gameObjectRequest.getText());
        String gameName = gameObjectRequest.getGameName();
        Game game = gameRepository.findByName(gameName).
                orElseGet(() -> gameRepository.save(Game.builder().name(gameName).build()));
        gameObject.setGame(game);
        gameObject.setUpdatedAt(LocalDate.now());
        return gameObjectMapper.entityToResponse(gameObjectRepository.save(gameObject));
    }

    @Override
    public void deleteById(Long id) {
        gameObjectRepository.deleteById(id);
    }
}

package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.dto.mapper.CommentMapper;
import com.leverx.dealerstat.dto.request.CommentRequest;
import com.leverx.dealerstat.dto.response.CommentResponse;
import com.leverx.dealerstat.entity.Comment;
import com.leverx.dealerstat.repository.CommentRepository;
import com.leverx.dealerstat.repository.UserRepository;
import com.leverx.dealerstat.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final CommentMapper commentMapper;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, CommentMapper commentMapper, UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
        this.userRepository = userRepository;
    }

    @Override
    public List<CommentResponse> getAllComments() {
        return commentMapper.entityToResponseList(commentRepository.findAll());
    }

    @Override
    public List<CommentResponse> getAllApprovedCommentsByTraderId(Long traderId) {
        return commentMapper.entityToResponseList(commentRepository.findAllByTraderIdAndApproved(traderId, true));
    }

    @Override
    public List<CommentResponse> getAllUnapprovedCommentsByTraderId(Long traderId) {
        return commentMapper.entityToResponseList(commentRepository.findAllByTraderIdAndApproved(traderId, false));
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public CommentResponse getById(Long id) {
        return commentMapper.entityToResponse(commentRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public CommentResponse saveNew(CommentRequest commentRequest) {
        Comment comment = commentMapper.requestToEntity(commentRequest);
        comment.setApproved(false);
        comment.setCreatedAt(LocalDate.now());
        comment.setTrader(userRepository.findById(commentRequest.getTraderId()).orElseThrow(EntityNotFoundException::new));
        return commentMapper.entityToResponse(commentRepository.save(comment));
    }

    @Override
    public CommentResponse update(Comment comment) {
        return commentMapper.entityToResponse(commentRepository.save(comment));
    }

    @Override
    public void approveById(Long id) {
        Comment comment = commentRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        comment.setApproved(true);
        commentRepository.save(comment);
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
    }
}

package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.entity.Game;
import com.leverx.dealerstat.repository.GameRepository;
import com.leverx.dealerstat.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.List;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;

    @Autowired
    public GameServiceImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public Game getById(Long id) {
        return gameRepository.findById(id).orElseThrow(EntityExistsException::new);
    }

    @Override
    public List<Game> getAll() {
        return gameRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        gameRepository.deleteById(id);
    }

    @Override
    public Game save(Game game) {
        return gameRepository.save(game);
    }
}

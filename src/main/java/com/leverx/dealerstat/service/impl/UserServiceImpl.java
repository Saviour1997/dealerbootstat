package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.dto.mapper.UserMapper;
import com.leverx.dealerstat.dto.request.UserRequest;
import com.leverx.dealerstat.dto.response.CommentResponse;
import com.leverx.dealerstat.dto.response.TraderRatingResponse;
import com.leverx.dealerstat.dto.response.UserResponse;
import com.leverx.dealerstat.entity.User;
import com.leverx.dealerstat.entity.enums.Role;
import com.leverx.dealerstat.repository.RedisUserRepository;
import com.leverx.dealerstat.repository.UserRepository;
import com.leverx.dealerstat.service.CommentService;
import com.leverx.dealerstat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RedisUserRepository redisRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final CommentService commentService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RedisUserRepository redisRepository, UserMapper userMapper, PasswordEncoder passwordEncoder, CommentService commentService) {
        this.userRepository = userRepository;
        this.redisRepository = redisRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.commentService = commentService;
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public UserResponse getById(Long id) {
        return userMapper.entityToResponse(userRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public List<UserResponse> getAllTraders() {
        return userMapper.entityToResponseList(userRepository.findAllByRole(Role.ROLE_TRADER));
    }

    @Override
    public User saveNew(UserRequest userRequest) {
        User user = userMapper.requestToEntity(userRequest);
        user.setEnabled(false);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.ROLE_ADMIN);
        return userRepository.save(user);
    }

    @Override
    public UserResponse update(User user) {
        User oldUser = userRepository.findById(user.getId()).orElseThrow(EntityNotFoundException::new);
        if (!user.getPassword().equals(oldUser.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return userMapper.entityToResponse(userRepository.save(user));
    }

    @Override
    public UserResponse saveConfirmed(User user) {
        user.setCreatedAt(LocalDate.now());
        user.setRole(Role.ROLE_TRADER);
        user.setEnabled(true);
        userRepository.findByEmail(user.getEmail())
                .ifPresentOrElse(
                        oldUser -> user.setId(oldUser.getId()),
                        () -> {
                            throw new EntityNotFoundException();
                        }
                );
        return userMapper.entityToResponse(userRepository.save(user));
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public String saveInRedis(User user) {
        return redisRepository.saveUser(user);
    }

    @Override
    public User findByTokenFromRedis(String token) {
        return redisRepository.getUserByToken(token).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void deleteByTokenFromRedis(String token) {
        redisRepository.deleteUserByToken(token);
    }

    @Override
    public boolean isEnabled(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
        return user.isEnabled();
    }

    @Override
    public double getTraderRatingById(Long id) {
        userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<CommentResponse> comments = commentService.getAllApprovedCommentsByTraderId(id);
        return comments.stream().mapToInt(CommentResponse::getRating).average().orElse(-1);
    }

    @Override
    public List<TraderRatingResponse> getTraderTopByRatings() {
        List<TraderRatingResponse> users = userMapper.tradersToTraderRatings(getAllTraders());
        return users.stream()
                .peek(userResponse -> userResponse.setRating(getTraderRatingById(userResponse.getId())))
                .filter(userResponse -> (userResponse.getRating() > 0))
                .sorted(Comparator.comparingDouble(TraderRatingResponse::getRating))
                .collect(Collectors.toList());
    }

}

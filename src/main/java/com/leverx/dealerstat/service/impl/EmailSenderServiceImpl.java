package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.constants.EmailMessageConst;
import com.leverx.dealerstat.service.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    private final JavaMailSender javaMailSender;

    private final String CONFIRM_EMAIL_URL;

    private final String CONFIRM_NEW_PASSWORD_URL;

    @Autowired
    public EmailSenderServiceImpl(JavaMailSender javaMailSender,
                                  @Value("${app.confirmEmailURL}") String confirm_email_url,
                                  @Value("${app.confirmNewPasswordURL}") String confirm_new_password_url) {
        this.javaMailSender = javaMailSender;
        CONFIRM_EMAIL_URL = confirm_email_url;
        CONFIRM_NEW_PASSWORD_URL = confirm_new_password_url;
    }

    @Override
    @Async
    public void sendEmail(SimpleMailMessage message) {
        javaMailSender.send(message);
    }

    @Override
    public SimpleMailMessage createConfirmSignUpMessage(String toEmail, String token) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(toEmail);
        mailMessage.setSubject(EmailMessageConst.REGISTER);
        mailMessage.setText(EmailMessageConst.REGISTER_TEXT + CONFIRM_EMAIL_URL + token);
        return mailMessage;
    }

    @Override
    public SimpleMailMessage createConfirmNewPasswordMessage(String toEmail, String token) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(toEmail);
        mailMessage.setSubject(EmailMessageConst.NEW_PASSWORD);
        mailMessage.setText(EmailMessageConst.PASSWORD_TEXT + CONFIRM_NEW_PASSWORD_URL + token);
        return mailMessage;
    }
}

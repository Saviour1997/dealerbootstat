package com.leverx.dealerstat.service;

import com.leverx.dealerstat.entity.Game;

import java.util.List;

public interface GameService {
    Game getById(Long id);

    List<Game> getAll();

    void deleteById(Long id);

    Game save(Game game);
}

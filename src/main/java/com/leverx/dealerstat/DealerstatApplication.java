package com.leverx.dealerstat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DealerstatApplication {

	public static void main(String[] args) {
		SpringApplication.run(DealerstatApplication.class, args);
	}

}

package com.leverx.dealerstat.config;

import com.leverx.dealerstat.authentication.UserDetailsServiceImpl;
import com.leverx.dealerstat.authentication.jwt.EntryPoint;
import com.leverx.dealerstat.authentication.jwt.JWTFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsServiceImpl userDetailsService;
    private final JWTFilter JWTFilter;
    private final EntryPoint entryPoint;

    @Autowired
    public WebSecurityConfig(UserDetailsServiceImpl userDetailsService, JWTFilter JWTFilter, EntryPoint entryPoint) {
        this.userDetailsService = userDetailsService;
        this.JWTFilter = JWTFilter;
        this.entryPoint = entryPoint;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(entryPoint)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.DELETE, "/api/users/{id}/comments/{commentId}")
                .authenticated()
                .antMatchers(HttpMethod.POST, "/api/users/{id}/make-admin")
                .authenticated()
                .antMatchers(HttpMethod.GET, "/api/users/{id}/comments/unapproved")
                .authenticated()
                .antMatchers("/api/users/{id}/comments/{commentId}/approve", "/api/users/{id}/comments/{commentId}/decline")
                .authenticated()
                .antMatchers("/api/auth/**", "/api/users/**")
                .anonymous();
        http.addFilterBefore(JWTFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}

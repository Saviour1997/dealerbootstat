package com.leverx.dealerstat.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TraderRatingResponse {
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate createdAt;

    private double rating;
}

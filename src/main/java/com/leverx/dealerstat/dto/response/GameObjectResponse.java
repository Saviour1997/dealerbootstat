package com.leverx.dealerstat.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GameObjectResponse {
    private Long id;

    private String title;

    private String text;

    private UserResponse trader;

    private String game;

    private LocalDate createdAt;

    private LocalDate updatedAt;
}

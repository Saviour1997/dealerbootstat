package com.leverx.dealerstat.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentResponse {
    private Long id;

    private int rating;

    private String message;

    private LocalDate createdAt;

    private boolean approved;

    private UserResponse trader;
}

package com.leverx.dealerstat.dto.response;

import com.leverx.dealerstat.constants.SecurityConst;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class JwtResponse {
    @NotNull
    private String token;

    @NotNull
    private final String type = SecurityConst.TOKEN_PREFIX;

    @NotNull
    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String email;

    @NotNull
    private List<String> roles;

    public JwtResponse(String jwt, Long id, String firstName, String lastName, String email, List<String> roles) {
        this.token = jwt;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.roles = roles;
    }
}

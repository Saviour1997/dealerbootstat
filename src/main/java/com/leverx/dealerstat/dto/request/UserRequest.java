package com.leverx.dealerstat.dto.request;

import com.leverx.dealerstat.constants.EntityConst;
import lombok.Data;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserRequest {

  private Long id;

  @NotBlank
  @Size(max = EntityConst.FIRST_NAME_MAX)
  private String firstName;

  @NotBlank
  @Size(max = EntityConst.LAST_NAME_MAX)
  private String lastName;

  @NotBlank
  @Size(min = EntityConst.PASSWORD_MIN, max = EntityConst.PASSWORD_MAX)
  private String password;

  @NotBlank
  @Size(max = EntityConst.EMAIL_MAX)
  @Email
  private String email;

  boolean enabled;

}

package com.leverx.dealerstat.dto.request;

import com.leverx.dealerstat.constants.EntityConst;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentRequest {
    private Long id;

    @NotNull
    @Min(EntityConst.RATING_MIN)
    @Max(EntityConst.RATING_MAX)
    private int rating;

    @Size(max = EntityConst.COMMENT_MESSAGE_MAX)
    private String message;

    private Long traderId;
}

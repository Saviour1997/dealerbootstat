package com.leverx.dealerstat.dto.request;

import com.leverx.dealerstat.constants.EntityConst;
import lombok.Data;
import lombok.Setter;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class GameObjectRequest {

    private Long id;

    @NotBlank
    @Size(max = EntityConst.TITLE_MAX)
    private String title;

    @Size(max = EntityConst.GAME_OBJECT_TEXT_MAX)
    private  String text;

    private  Long traderId;

    @Size(max = EntityConst.GAME_NAME_MAX)
    private String gameName;
}

package com.leverx.dealerstat.dto.request;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Value
public class LoginRequest {

  @NotBlank
  String password;

  @NotBlank
  @Email
  String email;
}

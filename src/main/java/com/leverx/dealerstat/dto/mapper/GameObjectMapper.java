package com.leverx.dealerstat.dto.mapper;

import com.leverx.dealerstat.dto.request.GameObjectRequest;
import com.leverx.dealerstat.dto.response.GameObjectResponse;
import com.leverx.dealerstat.entity.GameObject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GameObjectMapper {

    GameObject requestToEntity(GameObjectRequest gameObjectRequest);

    @Mappings({
            @Mapping(target = "game", source = "game.name")
    })
    GameObjectResponse entityToResponse(GameObject gameObject);

    List<GameObjectResponse> entityToResponseList(List<GameObject> gameObjects);
}

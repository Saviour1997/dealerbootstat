package com.leverx.dealerstat.dto.mapper;

import com.leverx.dealerstat.dto.request.UserRequest;
import com.leverx.dealerstat.dto.response.TraderRatingResponse;
import com.leverx.dealerstat.dto.response.UserResponse;
import com.leverx.dealerstat.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User requestToEntity(UserRequest user);

    UserResponse entityToResponse(User user);

    List<UserResponse> entityToResponseList(List<User> users);

    List<TraderRatingResponse> tradersToTraderRatings(List<UserResponse> traders);
}

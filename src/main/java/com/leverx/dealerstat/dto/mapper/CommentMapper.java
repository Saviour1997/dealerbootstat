package com.leverx.dealerstat.dto.mapper;

import com.leverx.dealerstat.dto.request.CommentRequest;
import com.leverx.dealerstat.dto.response.CommentResponse;
import com.leverx.dealerstat.entity.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentMapper {

    Comment requestToEntity(CommentRequest commentRequest);

    CommentResponse entityToResponse(Comment comment);

    List<CommentResponse> entityToResponseList(List<Comment> comments);
}

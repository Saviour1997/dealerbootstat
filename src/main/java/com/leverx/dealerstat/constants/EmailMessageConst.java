package com.leverx.dealerstat.constants;

public class EmailMessageConst {
    public static final String REGISTER = "Registration confirmation";
    public static final String PASSWORD_TEXT = "To confirm your new password, please click the link below: \n";
    public static final String REGISTER_TEXT = "To confirm your registration, please click the link below: \n";
    public static final String NEW_PASSWORD = "Password change confirmation";

}

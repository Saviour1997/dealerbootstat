package com.leverx.dealerstat.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EntityConst {

  public static final int FIRST_NAME_MAX = 20;
  public static final int LAST_NAME_MAX = 20;
  public static final int PASSWORD_MIN = 4;
  public static final int PASSWORD_MAX = 16;
  public static final int EMAIL_MAX = 50;
  public static final int RATING_MIN = 1;
  public static final int RATING_MAX = 5;
  public static final int COMMENT_MESSAGE_MAX = 500;
  public static final int TITLE_MAX = 50;
  public static final int GAME_NAME_MAX = 50;
  public static final int GAME_OBJECT_TEXT_MAX = 1000;
}

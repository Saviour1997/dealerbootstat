package com.leverx.dealerstat.constants;

public class ResponseMessageConst {
    //---auth---
    public static final String ENTITY_NOT_FOUND = "Can't found any entity";
    public static final String REGISTRATION_INCOMPLETE = "Please confirm your email to complete registration";
    public static final String INVALID_LINK = "This link is no longer valid, please register again";
    public static final String INVALID_PASSWORD_LINK = "The link is no longer valid";
    public static final String EMAIL_EXCEPTION = "Unable to send email";
    public static final String CONFIRM_PASSWORD_CHANGE = "Please confirm password";
    public static final String PASSWORD_CHANGED = "Password is successfully changed";
    public static final String EMAIL_IN_USE = "Error: Email is already in use";
    //---user---
    public static final String ROLE_CHANGE_SUCCESS = "Role is successfully changed";
    //---comment---
    public static final String COMMENT_DELETED = "Comment is successfully deleted";
    public static final String COMMENT_APPROVED = "Comment is successfully approved";
    public static final String COMMENT_DECLINED = "Comment is successfully declined";
    //---gameObject---
    public static final String GAME_OBJECT_DELETED = "Game object is successfully deleted";
    public static final String GAME_OBJECT_APPROVED = "Game object is successfully approved";
    public static final String GAME_OBJECT_DECLINED = "Game object is successfully declined";

}

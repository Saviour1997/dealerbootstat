package com.leverx.dealerstat.exception;

import com.leverx.dealerstat.constants.ResponseMessageConst;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
@ResponseBody
public class ControllerExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String entityNotFoundException() {
        return ResponseMessageConst.ENTITY_NOT_FOUND;
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String usernameNotFound(UsernameNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(MailException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String connectionException() {
        return ResponseMessageConst.EMAIL_EXCEPTION;
    }
}

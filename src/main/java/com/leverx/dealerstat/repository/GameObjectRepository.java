package com.leverx.dealerstat.repository;

import com.leverx.dealerstat.entity.GameObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameObjectRepository extends JpaRepository<GameObject, Long> {
    List<GameObject> findAllByTraderId(Long id);

    List<GameObject> findAllByGameId(Long id);
}

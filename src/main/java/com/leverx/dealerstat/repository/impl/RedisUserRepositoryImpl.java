package com.leverx.dealerstat.repository.impl;

import com.leverx.dealerstat.constants.SecurityConst;
import com.leverx.dealerstat.entity.User;
import com.leverx.dealerstat.repository.RedisUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisUserRepositoryImpl implements RedisUserRepository {

    private final RedisTemplate<String, Object> template;
    private ValueOperations<String, Object> valueOperations;

    @Autowired
    public RedisUserRepositoryImpl(RedisTemplate<String, Object> template) {
        this.template = template;
    }

    @PostConstruct
    private void init() {
        valueOperations = template.opsForValue();
    }

    @Override
    public String saveUser(User user) {
        String token = generateToken();
        template.expire(token, SecurityConst.EXPIRATION_TIME_IN_HOURS, TimeUnit.HOURS);
        valueOperations.set(token, user);
        return token;
    }

    @Override
    public Optional<User> getUserByToken(String token) {
        return Optional.ofNullable((User) valueOperations.get(token));
    }

    @Override
    public void deleteUserByToken(String token) {
        valueOperations.getOperations().delete(token);
    }

    private String generateToken() {
        String token;
        do {
            token = UUID.randomUUID().toString();
        } while (getUserByToken(token).isPresent());
        return token;
    }
}

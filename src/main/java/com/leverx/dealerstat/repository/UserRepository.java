package com.leverx.dealerstat.repository;

import com.leverx.dealerstat.entity.User;
import com.leverx.dealerstat.entity.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

    boolean existsByEmail(String email);

    void deleteByEmail(String email);

    List<User> findAllByRole(Role role);
}

package com.leverx.dealerstat.repository;


import com.leverx.dealerstat.entity.User;

import java.util.Optional;

public interface RedisUserRepository {
    String saveUser(User user);

    Optional<User> getUserByToken(String token);

    void deleteUserByToken(String token);
}

package com.leverx.dealerstat.repository;

import com.leverx.dealerstat.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByTraderId(Long id);

    List<Comment> findAllByTraderIdAndApproved(Long id, boolean approved);
}

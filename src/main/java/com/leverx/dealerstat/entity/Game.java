package com.leverx.dealerstat.entity;

import com.leverx.dealerstat.constants.EntityConst;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "game",
        uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class Game implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = EntityConst.GAME_NAME_MAX)
    private String name;
}

package com.leverx.dealerstat.entity.enums;

import java.io.Serializable;

public enum Role implements Serializable {
  ROLE_TRADER,
  ROLE_ADMIN
}

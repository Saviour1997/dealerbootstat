package com.leverx.dealerstat.entity;

import com.leverx.dealerstat.constants.EntityConst;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Min(EntityConst.RATING_MIN)
    @Max(EntityConst.RATING_MAX)
    private int rating;

    @Size(max = EntityConst.COMMENT_MESSAGE_MAX)
    private String message;

    private LocalDate createdAt;

    private boolean approved;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trader_id")
    private User trader;
}

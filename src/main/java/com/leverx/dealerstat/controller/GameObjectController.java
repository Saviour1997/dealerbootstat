package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.authentication.UserDetailsImpl;
import com.leverx.dealerstat.constants.ResponseMessageConst;
import com.leverx.dealerstat.dto.request.GameObjectRequest;
import com.leverx.dealerstat.dto.response.GameObjectResponse;
import com.leverx.dealerstat.service.GameObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class GameObjectController {

    private final GameObjectService gameObjectService;

    @Autowired
    public GameObjectController(GameObjectService gameObjectService) {
        this.gameObjectService = gameObjectService;
    }

    @GetMapping("/object")
    public ResponseEntity<?> getAllObjects() {
        List<GameObjectResponse> gameObjects = gameObjectService.getAll();
        if (gameObjects.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.ok(gameObjects);
        }
    }

    @Secured("ROLE_TRADER")
    @PostMapping("/object")
    public ResponseEntity<?> addObject(@Valid @RequestBody GameObjectRequest gameObjectRequest, @AuthenticationPrincipal UserDetailsImpl userDetails) {
        gameObjectRequest.setTraderId(userDetails.getId());
        return new ResponseEntity<>(gameObjectService.saveNew(gameObjectRequest), HttpStatus.CREATED);
    }

    @Secured("ROLE_TRADER")
    @PutMapping("/object/{id}")
    public ResponseEntity<?> updateObject(@PathVariable Long id, @Valid @RequestBody GameObjectRequest gameObjectRequest, @AuthenticationPrincipal UserDetailsImpl userDetails) {
        if (userDetails.getId().equals(gameObjectService.getById(id).getTrader().getId())) {
            gameObjectRequest.setId(id);
            return ResponseEntity.ok(gameObjectService.update(gameObjectRequest));
        } else return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @Secured("ROLE_TRADER")
    @GetMapping("/my")
    public ResponseEntity<?> getCurrentUserObjects(@AuthenticationPrincipal UserDetailsImpl userDetails) {
        return ResponseEntity.ok(gameObjectService.getAllByTraderId(userDetails.getId()));
    }

    @Secured("ROLE_TRADER")
    @DeleteMapping("/object/{id}")
    public ResponseEntity<?> deleteObject(@PathVariable Long id, @AuthenticationPrincipal UserDetailsImpl userDetails) {
        if (userDetails.getId().equals(gameObjectService.getById(id).getTrader().getId())) {
            gameObjectService.deleteById(id);
            return ResponseEntity.ok(ResponseMessageConst.GAME_OBJECT_DELETED);
        } else return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

}

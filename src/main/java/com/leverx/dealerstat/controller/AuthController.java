package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.authentication.UserDetailsImpl;
import com.leverx.dealerstat.authentication.jwt.JwtUtils;
import com.leverx.dealerstat.constants.ResponseMessageConst;
import com.leverx.dealerstat.dto.mapper.UserMapper;
import com.leverx.dealerstat.dto.request.LoginRequest;
import com.leverx.dealerstat.dto.request.UserRequest;
import com.leverx.dealerstat.dto.response.JwtResponse;
import com.leverx.dealerstat.dto.response.UserResponse;
import com.leverx.dealerstat.entity.User;
import com.leverx.dealerstat.service.EmailSenderService;
import com.leverx.dealerstat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final UserService userService;
    private final EmailSenderService emailService;
    private final AuthenticationManager authManager;
    private final JwtUtils jwtUtils;

    @Autowired
    public AuthController(UserService userService, EmailSenderService emailService, AuthenticationManager authManager, JwtUtils jwtUtils, UserMapper userMapper) {
        this.userService = userService;
        this.emailService = emailService;
        this.authManager = authManager;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<String> registerUser(@Valid @RequestBody UserRequest userRequest) {
        if (userService.existsByEmail(userRequest.getEmail()) && !userService.isEnabled(userRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(ResponseMessageConst.EMAIL_IN_USE);
        }
        User user = userService.saveNew(userRequest);
//        String token = userService.saveInRedis(user);
//        emailService.sendEmail(emailService.createConfirmSignUpMessage(userRequest.getEmail(), token));
        return ResponseEntity.ok(ResponseMessageConst.REGISTRATION_INCOMPLETE);
    }


    @GetMapping("/confirm/{token}")
    public ResponseEntity<?> confirmSignUp(@PathVariable String token) {
        User user;
        try {
            user = userService.findByTokenFromRedis(token);
        } catch (EntityNotFoundException ex) {
            userService.deleteByTokenFromRedis(token);
            return ResponseEntity
                    .badRequest()
                    .body(ResponseMessageConst.INVALID_LINK);
        }
        UserResponse userResponse = userService.saveConfirmed(user);
        userService.deleteByTokenFromRedis(token);
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest userRequest) {
        if (!userService.isEnabled(userRequest.getEmail())) {
            return new ResponseEntity<>(ResponseMessageConst.REGISTRATION_INCOMPLETE, HttpStatus.UNAUTHORIZED);
        }
        Authentication authentication = authManager.authenticate(
                new UsernamePasswordAuthenticationToken(userRequest.getEmail(), userRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getFirstName(),
                userDetails.getLastName(),
                userDetails.getEmail(),
                roles
        ));
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<?> changePassword(@Valid @RequestBody String email) {
        User user = userService.findByEmail(email);
        String token = userService.saveInRedis(user);
        emailService.sendEmail(emailService.createConfirmNewPasswordMessage(user.getEmail(), token));
        return ResponseEntity.ok(ResponseMessageConst.CONFIRM_PASSWORD_CHANGE);
    }

    @PostMapping("/reset/{token}")
    public ResponseEntity<?> resetPassword(@PathVariable String token, @RequestBody String password) {
        User user;
        try {
            user = userService.findByTokenFromRedis(token);
        } catch (EntityNotFoundException ex) {
            userService.deleteByTokenFromRedis(token);
            return ResponseEntity
                    .badRequest()
                    .body(ResponseMessageConst.INVALID_PASSWORD_LINK);
        }
        user.setPassword(password);
        userService.update(user);
        userService.deleteByTokenFromRedis(token);
        return new ResponseEntity<>(ResponseMessageConst.PASSWORD_CHANGED, HttpStatus.OK);
    }
}

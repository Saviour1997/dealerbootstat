package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.entity.Game;
import com.leverx.dealerstat.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/games")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("")
    public ResponseEntity<?> getAllGames() {
        List<Game> games = gameService.getAll();
        if (games.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.ok(games);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> addGame(@Valid @RequestBody Game game) {
        return ResponseEntity.ok(gameService.save(game));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateGame(@PathVariable Long id, @RequestBody String name) {
        Game game = gameService.getById(id);
        game.setName(name);
        return ResponseEntity.ok(gameService.save(game));
    }
}

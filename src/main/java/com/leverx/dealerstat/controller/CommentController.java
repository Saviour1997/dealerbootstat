package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.constants.ResponseMessageConst;
import com.leverx.dealerstat.dto.request.CommentRequest;
import com.leverx.dealerstat.dto.response.CommentResponse;
import com.leverx.dealerstat.entity.Comment;
import com.leverx.dealerstat.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/users/{id}/comments")
    public ResponseEntity<?> getTraderComments(@PathVariable Long id) {
        List<CommentResponse> comments = commentService.getAllApprovedCommentsByTraderId(id);
        if (comments.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.ok(comments);
        }
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/users/{id}/comments/unapproved")
    public ResponseEntity<?> getTraderUnapprovedComments(@PathVariable Long id) {
        List<CommentResponse> comments = commentService.getAllUnapprovedCommentsByTraderId(id);
        if (comments.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.ok(comments);
        }
    }

    @PostMapping("/users/{id}/comments")
    public ResponseEntity<?> addComment(@PathVariable Long id, @Valid @RequestBody CommentRequest commentRequest) {
        commentRequest.setTraderId(id);
        return new ResponseEntity<>(commentService.saveNew(commentRequest), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}/comments/{commentId}")
    public ResponseEntity<?> updateComment(@PathVariable Long id, @PathVariable Long commentId, @Valid @RequestBody CommentRequest commentRequest) {
        Comment comment = commentService.findById(commentId);
        comment.setRating(commentRequest.getRating());
        comment.setMessage(commentRequest.getMessage());
        return ResponseEntity.ok(commentService.update(comment));
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/users/{id}/comments/{commentId}/approve")
    public ResponseEntity<?> approveComment(@PathVariable("id") Long traderId, @PathVariable Long commentId) {
        commentService.approveById(commentId);
        return ResponseEntity.ok(ResponseMessageConst.COMMENT_APPROVED);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/users/{id}/comments/{commentId}/decline")
    public ResponseEntity<?> declineComment(@PathVariable("id") Long traderId, @PathVariable Long commentId) {
        commentService.deleteById(commentId);
        return ResponseEntity.ok(ResponseMessageConst.COMMENT_DECLINED);
    }

    @GetMapping("/users/{id}/comments/{commentId}")
    public ResponseEntity<?> getComment(@PathVariable("id") Long traderId, @PathVariable Long commentId) {
        return ResponseEntity.ok(commentService.getById(commentId));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/users/{id}/comments/{commentId}")
    public ResponseEntity<?> deleteComment(@PathVariable("id") Long traderId, @PathVariable Long commentId) {
        commentService.deleteById(commentId);
        return ResponseEntity.ok(ResponseMessageConst.COMMENT_DELETED);
    }

}

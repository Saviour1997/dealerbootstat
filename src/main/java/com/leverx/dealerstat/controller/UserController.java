package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.constants.ResponseMessageConst;
import com.leverx.dealerstat.dto.response.UserResponse;
import com.leverx.dealerstat.entity.User;
import com.leverx.dealerstat.entity.enums.Role;
import com.leverx.dealerstat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/{id}/make-admin")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> makeAdmin(@PathVariable Long id) {
        User user = userService.findById(id);
        user.setRole(Role.ROLE_ADMIN);
        userService.update(user);
        return ResponseEntity.ok(ResponseMessageConst.ROLE_CHANGE_SUCCESS);
    }

    @GetMapping("")
    public ResponseEntity<?> getAllUsers() {
        List<UserResponse> users = userService.getAllTraders();
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.ok(users);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getById(id));
    }

    @GetMapping("/{id}/rating")
    public ResponseEntity<?> getTraderRating(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getTraderRatingById(id));
    }

    @GetMapping("/top")
    public ResponseEntity<?> getTradersTop() {
        return ResponseEntity.ok(userService.getTraderTopByRatings());
    }
}
